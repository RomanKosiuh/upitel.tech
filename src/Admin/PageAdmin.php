<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class PageAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper->add('slug', TextType::class, [
            'label' => 'URL'
        ]);
        $formMapper->add('title', TextType::class, [
            'label' => 'Название'
        ]);
        $formMapper->add('text', TextareaType::class, [
            'label' => 'Текст',
            'required' => false
        ]);
        $formMapper->add('meta_title', TextType::class, [
            'label' => 'Meta title',
            'required' => false
        ]);
        $formMapper->add('meta_desc', TextType::class, [
            'label' => 'Meta description',
            'required' => false
        ]);
        $formMapper->add('meta_keywords', TextType::class, [
            'label' => 'Meta keywords',
            'required' => false
        ]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('title');
        $datagridMapper->add('slug');
        $datagridMapper->add('text');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('title', null, [
            'label' => 'Название'
        ]);
        $listMapper->addIdentifier('slug', null, [
            'label' => 'URL'
        ]);
    }
}