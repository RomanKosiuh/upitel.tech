<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class WorkAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {

    	$service = $this->getSubject();

        $fileFieldOptions = [
        	'required' => false,
        	'data_class' => null,
            'label' => 'Изображение'
        ];
        if ($service && property_exists($service, 'image') && $service->getImage() !== null) {
            $container = $this->getConfigurationPool()->getContainer();
            $fullPath = $container->get('request_stack')->getCurrentRequest()->getBasePath().'/uploads/'.$service->getImage();

            $fileFieldOptions['help'] = '<img src="'.$fullPath.'" class="admin-preview" />';
        }

        $formMapper->add('name', TextType::class, [
            'label' => 'Название'
        ]);
        $formMapper->add('description', TextareaType::class, [
            'label' => 'Описание',
            'required' => false
        ]);
        $formMapper->add('link', TextType::class, [
            'label' => 'Ссылка'
        ]);
        $formMapper->add('image', FileType::class, $fileFieldOptions);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name');
        $datagridMapper->add('description');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('name', null, [
            'label' => 'Название'
        ]);
        $listMapper->addIdentifier('description', null, [
            'label' => 'Описание'
        ]);
        $listMapper->addIdentifier('link', null, [
            'label' => 'Ссылка'
        ]);
    }
}